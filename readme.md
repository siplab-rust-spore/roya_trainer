

## Flow training

Default pretrained weights
```
wget -c https://download.openmmlab.com/mmflow/gma/gma_8x2_120k_flyingchairs_368x496.pth
wget -c https://download.openmmlab.com/mmflow/raft/raft_8x2_100k_flyingchairs.pth
wget -c https://download.openmmlab.com/mmsegmentation/v0.5/convnext/upernet_convnext_base_fp16_640x640_160k_ade20k/upernet_convnext_base_fp16_640x640_160k_ade20k_20220227_182859-9280e39b.pth
wget -c https://download.openmmlab.com/mmsegmentation/v0.5/swin/upernet_swin_base_patch4_window12_512x512_160k_ade20k_pretrain_384x384_22K/upernet_swin_base_patch4_window12_512x512_160k_ade20k_pretrain_384x384_22K_20210531_125459-429057bf.pth 
```