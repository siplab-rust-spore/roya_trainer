#!/usr/bin/env python3
## Add dataset

import argparse
import os
import pathlib
import os.path as osp
import shutil
import mmcv


def convert_dataset(dataset_path, output_path):
    img_dir = f"{output_path}/images"
    label_dir = f"{output_path}/labels"

    # Create images & annotation dir
    pathlib.Path(img_dir).mkdir(parents=True, exist_ok=True)
    pathlib.Path(label_dir).mkdir(parents=True, exist_ok=True)

    filename_list = []

    # We'll move files to 2 directories images & lavels
    # We'll also convert labels from grayscale images to rgb images

    for root, dirs, files in os.walk(dataset_path):
        for name in files:
            file_dir = None
            rgb_filename = None
            render_name = os.path.split(os.path.split(root)[0])[1]

            if name.startswith("Image"):
                file_dir = img_dir
                rgb_filename = name[5:]
                filename_list.append(render_name + "_" + rgb_filename[:4])
            elif name.startswith("Segmentation"):
                file_dir = label_dir
                rgb_filename = name[12:]
            else:
                continue

            file_path = os.path.join(root, name)
            # file_path_without_suffix = os.path.join(root, os.path.splitext(name))
            rgb_filename = render_name + "_" + rgb_filename
            rgb_file_path = os.path.join(file_dir, rgb_filename)

            shutil.copy(file_path, rgb_file_path)

    # split train/val set randomly
    split_dir = f'{output_path}/splits'

    train_percentage = 0.8

    mmcv.mkdir_or_exist(osp.join('.', split_dir))


    with open(osp.join(split_dir, 'train.txt'), 'w') as f:
        # select first 4/5 as train set
        train_length = int(len(filename_list)*train_percentage)
        f.writelines(line + '\n' for line in filename_list[:train_length])

    with open(osp.join(split_dir, 'val.txt'), 'w') as f:
        # select last 1/5 as train set
        f.writelines(line + '\n' for line in filename_list[train_length:])



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset_path", help="Provide path to the dataset", type=str, required=True)
    parser.add_argument(
        "--output_path", help="Provide path the output of the edited images", type=str, required=True
    )
    args = parser.parse_args()

    convert_dataset(args.dataset_path, args.output_path)
