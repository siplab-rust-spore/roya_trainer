#!/bin/bash

WHO=`whoami`
export PATH=/home/$WHO/anaconda3/condabin/:$PATH

eval "$(conda shell.bash hook)"
conda activate mmflow

cd /home/$WHO/work/roya_trainer

python mmseg_training.py \
 --dataset_path /home/$WHO/work/seg_dataset/ \
 --pretrained_load_from /home/$WHO/Downloads/upernet_convnext_base_fp16_640x640_160k_ade20k_20220227_182859-9280e39b.pth \
 --work_dir /home/$WHO/work/ \
 --config_file /home/$WHO/work/roya_trainer/cfg_files/convnext_8x2_roya_640x480.py \
 --training_name convnext_training_$(date -u +"%Y-%m-%d") \
 --splits_path /home/$WHO/work/seg_dataset/splits/ \
 --wandb_key /home/$WHO/work/roya_trainer/wandb.key \
 > /home/$WHO/training_convnext_$(date -u +"%Y-%m-%d").log 2>&1
