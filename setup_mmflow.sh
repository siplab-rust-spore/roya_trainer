#!/usr/bin/env bash

# Create an enviroment, with python between 3.6 and 3.9 otherwise the whl won't be available
# Newer versions of torch, mmcv and others might work, but havent been tested
# conda create --name mmcv python=3.9

## Install torch, torchvision, mmcv and mmflow

# Install PyTorch
pip install -U torch==1.8.0+cu111 torchvision==0.9.0+cu111 -f https://download.pytorch.org/whl/torch_stable.html
# Install MMCV
pip install mmcv-full -f https://download.openmmlab.com/mmcv/dist/cu111/torch1.8.0/index.html

pip install wandb
pip install git+https://github.com/georgegach/flowiz/

git clone https://github.com/open-mmlab/mmflow.git 
cd mmflow
pip install -e .
cd ..

git clone https://github.com/open-mmlab/mmsegmentation.git  
cd mmsegmentation
pip install -e .
cd ..