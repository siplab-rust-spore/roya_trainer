#!/bin/bash

WHO=`whoami`
export PATH=/home/$WHO/anaconda3/condabin/:$PATH

eval "$(conda shell.bash hook)"
conda activate mmflow

cd /home/$WHO/work/roya_trainer

python mmseg_training.py \
 --dataset_path /home/$WHO/work/seg_dataset/ \
 --pretrained_load_from /home/$WHO/Downloads/upernet_swin_base_patch4_window12_512x512_160k_ade20k_pretrain_384x384_22K_20210531_125459-429057bf.pth \
 --work_dir /home/$WHO/work/ \
 --config_file /home/$WHO/work/roya_trainer/cfg_files/swin_8x2_roya_640x480.py \
 --training_name swin_training_$(date -u +"%Y-%m-%d") \
 --splits_path /home/$WHO/work/seg_dataset/splits/ \
 --wandb_key /home/$WHO/work/roya_trainer/wandb.key \
 > /home/$WHO/training_swin_$(date -u +"%Y-%m-%d").log 2>&1
