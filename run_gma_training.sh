#!/bin/bash

WHO=`whoami`
export PATH=/home/$WHO/anaconda3/condabin/:$PATH

eval "$(conda shell.bash hook)"
conda activate mmflow

cd /home/$WHO/work/roya_trainer

python mmflow_training.py \
 --dataset_path /home/$WHO/work/flow_dataset/ \
 --pretrained_load_from /home/$WHO/Downloads/gma_8x2_120k_flyingchairs_368x496.pth \
 --work_dir /home/$WHO/work/ \
 --config_file /home/$WHO/work/roya_trainer/cfg_files/gma_8x2_roya_640x480.py \
 --training_name gma_training_$(date -u +"%Y-%m-%d") \
 --wandb_key /home/$WHO/work/roya_trainer/wandb.key \
 > /home/$WHO/training_gma_$(date -u +"%Y-%m-%d").log 2>&1
