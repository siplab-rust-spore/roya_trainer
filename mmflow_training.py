#!/usr/bin/env python3

import argparse
import wandb
from mmflow.apis import set_random_seed
from mmcv import Config
import mmcv
from mmflow.apis import train_model
from mmflow.datasets import build_dataset
from mmflow.models import build_flow_estimator
from os import path as osp
import sys
import os


def wandb_login(key_path, run_name):
     
    with open(key_path) as file:
        wandb_key = file.read()
    os.environ['WANDB_API_KEY'] = wandb_key

    wandb.login()

    # wandb.init(project="roya-flow", name=run_name)

class FlowTrainer():
    def __init__(self, dataset_path, config_file, load_from, resume_from, work_dir, training_name):
        self.dataset_path = dataset_path
        self.config_file = config_file
        self.load_from = load_from
        self.resume_from = resume_from
        self.work_dir = work_dir
        self.training_name = training_name

        self.dataset_num_files = 0
        for _, _, file_list in os.walk(f"{dataset_path}/training/clean/"):
            for file in file_list:
                if file.endswith(".png"):
                    self.dataset_num_files += 1

    def run_flow_training(self, config = {
        'batch_size' : 4,
        'optimizer' : 'AdamW',
        'max_lr' : 0.000125,
        'pct_start': 0.05,
        'epochs': 25
    }):
        mmcv_cfg = Config.fromfile(self.config_file)

        with wandb.init(project="roya-flow", name=self.training_name):
            if wandb.config != {}:
                config = wandb.config

            print(config)

            mmcv_cfg.load_from = self.load_from
            mmcv_cfg.resume_from = self.resume_from
            mmcv_cfg.work_dir = f'{self.work_dir}/{self.training_name}/'

            mmcv_cfg.data_root = self.dataset_path
            mmcv_cfg.data.train.data_root = self.dataset_path
            mmcv_cfg.data.val.data_root = self.dataset_path
            mmcv_cfg.data.test.data_root = self.dataset_path

            mmcv_cfg.data['train_dataloader'].samples_per_gpu = config.batch_size
            mmcv_cfg.data['val_dataloader'].samples_per_gpu = config.batch_size
            mmcv_cfg.data['test_dataloader'].samples_per_gpu = config.batch_size

            mmcv_cfg.optimizer['type'] = config.optimizer

            mmcv_cfg.runner['max_iters'] = int(self.dataset_num_files * config.epochs / config.batch_size)
            mmcv_cfg.checkpoint_config['interval'] = int(self.dataset_num_files / config.batch_size )
            mmcv_cfg.evaluation['interval'] = int(self.dataset_num_files / config.batch_size )

            mmcv_cfg.lr_config['max_lr'] = config.max_lr
            mmcv_cfg.lr_config['pct_start'] = config.pct_start


            print(f'Config:\n{mmcv_cfg.pretty_text}')

            """### Train and Evaluation"""
            # Build the dataset
            datasets = [build_dataset(mmcv_cfg.data.train)]

            # Build the detector
            model = build_flow_estimator(mmcv_cfg.model)

            # Create work_dir
            mmcv.mkdir_or_exist(osp.abspath(mmcv_cfg.work_dir))
            train_model(model, datasets, mmcv_cfg, distributed=False, validate=True, meta=dict())


    def run_sweep_flow_training(self):
        sweep_config = {
            'method': 'bayes',
            'metric' : {
                'name': 'val/EPE in Sintel clean subset',
                'goal': 'minimize'
            },
            'parameters': {
                'batch_size': {
                    'distribution': 'int_uniform',
                    'min': 1,
                    'max': 4
                },
                'optimizer': {
                    'values': ['AdamW']
                },
                'max_lr': {
                    'distribution': 'log_uniform_values',
                    'max': 0.0012500,
                    'min': 0.0000125
                },
                'pct_start' : {
                    'max': 0.20,
                    'min': 0.01
                },
                'epochs': {
                    'distribution': 'int_uniform',
                    'min': 1,
                    'max': 50
                }
            }
        }

        sweep_id = wandb.sweep(sweep_config, project="roya-flow")
        wandb.agent(sweep_id, self.run_flow_training, count=25)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dataset_path", help="Provide path to the dataset", type=str, required=True
    )
    parser.add_argument(
        "--pretrained_load_from",
        help="Pretrained weights if starting a new training",
        type=str,
        default=None
    )
    parser.add_argument(
        "--resume_from_training",
        help="Weights to resume from a previously interrupted training",
        type=str,
        default=None
    )
    parser.add_argument(
        "--work_dir",
        help="Working directory to save files and logs",
        type=str,
        required=True,
    )
    parser.add_argument("--training_name", help="Name for the training, this will correspond to naming for wandb, workdirs, etc", type=str)
    parser.add_argument(
        "--config_file", help="File to configure the network", type=str, required=True
    )
    parser.add_argument(
        "--wandb_key", help="Text file with the WandDB API key", type=str, required=True
    )
    parser.add_argument(
        "--run_sweep", help="Starts a sweep using wandb", action="store_true")
    args = parser.parse_args()

    wandb_login(args.wandb_key, args.training_name)

    flow_trainer = FlowTrainer(args.dataset_path, args.config_file, args.pretrained_load_from, args.resume_from_training, args.work_dir, args.training_name)
    if not args.run_sweep:
        flow_trainer.run_flow_training()
    else:
        flow_trainer.run_sweep_flow_training()

