#!/bin/bash

WHO=`whoami`
export PATH=/home/$WHO/anaconda3/condabin/:$PATH

eval "$(conda shell.bash hook)"
conda activate mmflow

cd /home/$WHO/work/roya_trainer

python mmflow_training.py \
 --dataset_path /home/$WHO/work/flow_dataset/ \
 --pretrained_load_from /home/$WHO/Downloads/raft_8x2_100k_flyingchairs.pth \
 --work_dir /home/$WHO/work/ \
 --config_file /home/$WHO/work/roya_trainer/cfg_files/raft_8x2_roya_640x480.py \
 --training_name raft_training_$(date -u +"%Y-%m-%d") \
 --wandb_key /home/$WHO/work/roya_trainer/wandb.key \
 --run_sweep \
 > /home/$WHO/training_raft_$(date -u +"%Y-%m-%d").log 2>&1
