#!/usr/bin/env python3

from os import path as osp
import argparse
import os
import sys

from mmcv import Config
from mmseg.apis import train_segmentor
from mmseg.datasets import build_dataset
from mmseg.datasets.builder import DATASETS
from mmseg.datasets.custom import CustomDataset
from mmseg.models import build_segmentor
from mmseg.utils import get_device
import mmcv
import wandb

import numpy as np
from PIL import Image

# define class and plaette for better visualization
classes = ('background', 'water', 'smoke', 'leaf')
palette = [[128, 128, 128], [128,0,0], [0,128,0], [0, 0, 128]]


@DATASETS.register_module()
class RoyaDataset(CustomDataset):
  CLASSES = classes
  PALETTE = palette
  def __init__(self, split, **kwargs):
    super().__init__(img_suffix='.png', seg_map_suffix='.png', 
                     split=split, **kwargs)
    assert osp.exists(self.img_dir) and self.split is not None



def wandb_login(key_path, run_name):
     
    with open(key_path) as file:
        wandb_key = file.read()
    os.environ['WANDB_API_KEY'] = wandb_key

    wandb.login()

    wandb.init(project="roya-segmentation", name=run_name)


def run_seg_training(dataset_path, config_file, load_from, resume_from, work_dir, training_name, split_path):
    cfg = Config.fromfile(config_file)

    cfg.device = get_device()

    cfg.load_from = load_from
    cfg.resume_from = resume_from
    cfg.work_dir = f'{work_dir}/{training_name}/'

    cfg.data_root = dataset_path
    cfg.data.train.data_root = dataset_path
    cfg.data.train.split = f"{split_path}/train.txt"
    cfg.data.val.data_root = dataset_path
    cfg.data.val.split = f"{split_path}/val.txt"
    cfg.data.test.data_root = dataset_path
    cfg.data.test.split = f"{split_path}/val.txt"

    # Rename WandBD log
    for hook in cfg.log_config.hooks:
        if hook.type == 'WandbLoggerHook':
            hook.init_kwargs.name = training_name

    # Build the dataset
    datasets = [build_dataset(cfg.data.train)]

    # Build the detector
    model = build_segmentor(
    cfg.model, train_cfg=cfg.get('train_cfg'), test_cfg=cfg.get('test_cfg'))
    # Add an attribute for visualization convenience
    model.CLASSES = datasets[0].CLASSES
    model.PALETTE = datasets[0].PALETTE

    # Create work_dir
    mmcv.mkdir_or_exist(osp.abspath(cfg.work_dir))
    train_segmentor(model, datasets, cfg, distributed=False, validate=True, 
                    meta=dict())


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dataset_path", help="Provide path to the dataset", type=str, required=True
    )
    parser.add_argument(
        "--pretrained_load_from",
        help="Pretrained weights if starting a new training",
        type=str,
        default=None
    )
    parser.add_argument(
        "--resume_from_training",
        help="Weights to resume from a previously interrupted training",
        type=str,
        default=None
    )
    parser.add_argument(
        "--work_dir",
        help="Working directory to save files and logs",
        type=str,
        required=True,
    )
    parser.add_argument("--training_name", help="Name for the training, this will correspond to naming for wandb, workdirs, etc", type=str)
    parser.add_argument(
        "--config_file", help="File to configure the network", type=str, required=True
    )
    parser.add_argument(
        "--wandb_key", help="Text file with the WandDB API key", type=str, required=True
    )
    parser.add_argument(
        "--splits_path", help="Directory with a train.txt and a val.txt with the names of the images/labels without an extension", type=str, required=True
    )
    args = parser.parse_args()

    wandb_login(args.wandb_key, args.training_name)
    run_seg_training(args.dataset_path, args.config_file, args.pretrained_load_from, args.resume_from_training, args.work_dir, args.training_name, args.splits_path)
