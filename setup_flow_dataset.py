#!/usr/bin/env python3
## Add dataset

import argparse
import os
import pathlib
from PIL import Image
import numpy as np
import shutil


def convert_dataset(dataset_path, output_path):
    img_dir = f"{output_path}/training/clean/"
    label_dir = f"{output_path}training/flow/"
    occlusion_dir = f"{output_path}training/occlusions/"
    invalid_dir = f"{output_path}training/invalid/"

    # Create images & annotation dir
    pathlib.Path(img_dir).mkdir(parents=True, exist_ok=True)
    pathlib.Path(label_dir).mkdir(parents=True, exist_ok=True)
    pathlib.Path(occlusion_dir).mkdir(parents=True, exist_ok=True)
    pathlib.Path(invalid_dir).mkdir(parents=True, exist_ok=True)

    filename_list = []

    # We'll move files to 2 directories images & lavels
    # We'll also convert labels from grayscale images to rgb images

    data_root = dataset_path

    resx = 640
    resy = 480

    for root, dirs, files in os.walk(data_root):
        for name in files:
            file_dir = None
            rgb_filename = None
            render_name = os.path.split(os.path.split(root)[0])[1]
            extension = ""

            if name.startswith("Image"):
                file_dir = img_dir
                rgb_filename = name[5:]
                extension = ".png"
                # filename_list.append(render_name + '_' + rgb_filename[:4])
            elif name.startswith("Vector"):
                file_dir = label_dir
                rgb_filename = name[6:]
                extension = ".flo"
            else:
                continue

            # Input filename
            file_path = os.path.join(root, name)

            # Output filename
            rgb_filename = render_name + "_" + rgb_filename
            rgb_filename = rgb_filename[6] + rgb_filename[8:12] + extension
            rgb_file_path = os.path.join(file_dir + "/" + render_name, rgb_filename)

            pathlib.Path(file_dir + "/" + render_name).mkdir(
                parents=True, exist_ok=True
            )

            # We copy either to .flo or .png to the endfile
            shutil.copy(file_path, rgb_file_path)
            # We also create a occulsion and invalid image files
            if extension == ".flo":
                # Just save all 0s as the occlusion image
                pathlib.Path(occlusion_dir + "/" + render_name).mkdir(
                    parents=True, exist_ok=True
                )
                occ_file_path = os.path.join(
                    occlusion_dir + "/" + render_name, rgb_filename[:5] + ".png"
                )
                occ_array = np.zeros((resy, resx), np.uint8)
                im = Image.fromarray(occ_array)
                im.save(occ_file_path)

                # Just save all 0s as the invalid image
                pathlib.Path(invalid_dir + "/" + render_name).mkdir(
                    parents=True, exist_ok=True
                )
                invalid_file_path = os.path.join(
                    invalid_dir + "/" + render_name, rgb_filename[:5] + ".png"
                )
                invalid_array = np.zeros((resy, resx), np.uint8)
                im = Image.fromarray(invalid_array)
                im.save(invalid_file_path)

    # We remove the last file from the annotations, occlusions, since the flow requires a "next" image, having this will cause an error when loading the dataset
    os.system(f"rm {output_path}/training/occlusions/**/*250.png")
    os.system(f"rm {output_path}/training/flow/**/*250.png")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset_path", help="Provide path to the dataset", type=str, required=True)
    parser.add_argument(
        "--output_path", help="Provide path the output of the edited images", type=str, required=True
    )
    args = parser.parse_args()

    convert_dataset(args.dataset_path, args.output_path)
